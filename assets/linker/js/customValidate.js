/**
 * Created by instancetype on 6/30/14.
 */
$(document).ready(function(){

    $('#signup-form').validate({
        rules: {

            firstName: {
                required: true
            },

            lastName: {
                required: true
            },

            email: {
                required: true,
                email: true
            },

            hoursAvailable: {
                 required: true,
                 digits: true
            }
        },

        success: function(element) {
            element.text('Great!').addClass('valid')
        }
    })

})
