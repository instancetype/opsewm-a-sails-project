/**
 * Member
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  schema: true

  ,
  attributes: { firstName: {
                  type: 'STRING'
                , required: true
                }
  	
              , lastName: {
                  type: 'STRING'
                , required: true
                }

              , email: {
                  type: 'STRING'
                , email: true
                , required: true
                , unique: true
                }

              , hoursAvailable: {
                  type: 'INTEGER'
                }

              , skills: {
                  type: 'TEXT'
                }

              , toJSON: function() {
                  var obj = this.toObject()
                  delete obj._csrf

                  return obj
                }
              }
}
