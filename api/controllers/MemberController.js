/**
 * MemberController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
  'new': function(req, res) {
    res.view()
  }

  ,
  create : function (req, res) {
    Member.create(req.params.all(), function memberCreated(err, member) {
      // if (err) return res.serverError(JSON.stringify(err));

      if (err) {
        // console.log(JSON.stringify(err));

        req.session.flash = {
          err : err
        };


        return res.redirect('/member/new');
      }
      // res.json(user);
      res.redirect('member/show/' + member.id);
    });
  }

  ,
  show: function(req, res, next) {
    Member.findOne(req.param('id'), function foundMember(err, member) {
      if (err) return next(err);
      if (!member) return next();

      res.view({
                 member : member
               });
    });
  }

  ,
  index: function(req, res, next) {
    Member.find(function foundMembers(err, members) {
      if (err) return next(err);

      res.view({
                 members: members
               });
    });
  }

  ,
  edit: function(req, res, next) {
    Member.findOne(req.param('id'), function foundMember(err, member) {
      if (err) return next(err);
      if (!member) return next('Member doesn\'t exist.');

      res.view({
                 member: member
               });
    });
  }

  ,
  update: function(req, res, next) {
    Member.update(req.param('id'), req.params.all(), function memberUpdated(err) {
      if (err) {
        return res.redirect('/member/edit/' + req.param('id'));
      }

      res.redirect('/member/show/' + req.param('id'));
    });
  }

  ,
  confirm: function(req, res, next) {
    res.view()
  }

  ,
  destroy: function(req, res, next) {
    Member.findOne(req.param('id'), function foundMember(err, member) {
      if (err) return next(err);
      if (!member) return next('Member doesn\'t exist.');

      Member.destroy(req.param('id'), function memberDestroyed(err) {
        if (err) return next(err);
      });
      res.redirect('/member');
    });
  }


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to MemberController)
   */
  ,
  _config: {}

  
};
